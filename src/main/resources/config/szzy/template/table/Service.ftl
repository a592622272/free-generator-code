package ${fileDirectoryServicePackage};

import ${fileDirectoryServiceAbstractServicePackage}.${className}ServiceAbstractService;
import ${fileDirectoryEntityPackage}.${className};

/**
* 表备注：${introspectedTable.remarks!}<#--deal null-->
* @description 针对表【${dataBase}.${introspectedTable.fullyQualifiedTable.introspectedTableName}】的Service
*
*/
public interface ${className}Service extends BaseService<${className}>, ${className}ServiceAbstractService {


}
