package ${fileDirectoryMapperPackage};

import ${fileDirectoryEntityPackage}.${className};

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 表备注：${introspectedTable.remarks!}<#--deal null-->
 * @description 针对表【${dataBase}.${introspectedTable.fullyQualifiedTable.introspectedTableName}】的Mapper
 *
 */
public interface ${className}Mapper extends BaseMapper<${className}> {


}




