package ${fileDirectoryEntityExtPackage};

import ${fileDirectoryEntityPackage}.${className};
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * 表备注：${introspectedTable.remarks!}
 * @description 针对表【${dataBase}.${introspectedTable.fullyQualifiedTable.introspectedTableName}】的扩展表
 *
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Slf4j
public class ${className}Ext extends ${className} implements Serializable {
    private static final long serialVersionUID = 1L;


}