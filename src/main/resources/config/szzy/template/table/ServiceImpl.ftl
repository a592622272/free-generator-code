package ${fileDirectoryServiceImplPackage};

import ${fileDirectoryServiceAbstractPackage}.${className}ServiceAbstract;
import ${fileDirectoryMapperPackage}.${className}Mapper;
import ${fileDirectoryServicePackage}.${className}Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 表备注：${introspectedTable.remarks!}<#--deal null-->
 * @description 针对表【${dataBase}.${introspectedTable.fullyQualifiedTable.introspectedTableName}】的Service实现
 */
@Service
public class ${className}ServiceImpl extends ${className}ServiceAbstract implements ${className}Service {


}
