package ${fileDirectoryServiceAbstractServicePackage};

import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import ${fileDirectoryEntityPackage}.${className};

import java.util.List;
import java.util.Map;
import java.util.Optional;

<#--

&lt;#&ndash; 导入包，去除重复包 &ndash;&gt;
<#list introspectedTable.allColumns as allColumns>
<#assign paramType=allColumns.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters />&lt;#&ndash;定义局部变量&ndash;&gt;
    <#if paramType ?index_of("java.lang")==-1 && paramType ?index_of("[]")==-1>&lt;#&ndash;java.lang的包不需要导入，数组也不要导入，比如byte[] &ndash;&gt;
        <#list introspectedTable.allColumns as allColumns2>
            <#if allColumns2.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters ?index_of("java.lang")!=0>
                <#if allColumns.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters ==allColumns2.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters>
                    <#if allColumns_index==allColumns2_index>
&lt;#&ndash; 导入包名 &ndash;&gt;
import ${allColumns.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters};
                    <#else>
                        <#break >
                    </#if>
                </#if>
            </#if>
        </#list>
    </#if>
</#list>

-->

/**
 *
 * 表备注：${introspectedTable.remarks!}<#--deal null-->
 * @description 针对表【${dataBase}.${introspectedTable.fullyQualifiedTable.introspectedTableName}】的ServiceAbstractService
 */
public interface ${className}ServiceAbstractService {

    int countBaseByMap(Map<SFunction<${className}, ?>, Object> map);
    Page<${className}> getBaseByMapPage(Page<${className}> page, Map<SFunction<${className}, ?>, Object> map);
    List<${className}> getBaseByMap(Map<SFunction<${className}, ?>, Object> map);
    Optional<${className}> getBaseByMapOneOp(Map<SFunction<${className}, ?>, Object> map);

    int countBaseByMapIgnoreNull(Map<SFunction<${className}, ?>, Object> map);
    Page<${className}> getBaseByMapPageIgnoreNull(Page<${className}> page, Map<SFunction<${className}, ?>, Object> map);
    List<${className}> getBaseByMapIgnoreNull(Map<SFunction<${className}, ?>, Object> map);
    Optional<${className}> getBaseByMapOneOpIgnoreNull(Map<SFunction<${className}, ?>, Object> map);


}



