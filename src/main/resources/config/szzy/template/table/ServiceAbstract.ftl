package ${fileDirectoryServiceAbstractPackage};

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;


import ${fileDirectoryServiceAbstractServicePackage}.${className}ServiceAbstractService;
import ${fileDirectoryMapperPackage}.${className}Mapper;
import com.cjbdi.yjdaxt.api.BaseService;
import ${fileDirectoryServicePackage}.${className}Service;
import ${fileDirectoryEntityPackage}.${className};

import org.apache.ibatis.exceptions.TooManyResultsException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

<#--

&lt;#&ndash; 导入包，去除重复包 &ndash;&gt;
<#list introspectedTable.allColumns as allColumns>
<#assign paramType=allColumns.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters />&lt;#&ndash;定义局部变量&ndash;&gt;
    <#if paramType ?index_of("java.lang")==-1 && paramType ?index_of("[]")==-1>&lt;#&ndash;java.lang的包不需要导入，数组也不要导入，比如byte[] &ndash;&gt;
        <#list introspectedTable.allColumns as allColumns2>
            <#if allColumns2.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters ?index_of("java.lang")!=0>
                <#if allColumns.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters ==allColumns2.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters>
                    <#if allColumns_index==allColumns2_index>
&lt;#&ndash; 导入包名 &ndash;&gt;
import ${allColumns.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters};
                    <#else>
                        <#break >
                    </#if>
                </#if>
            </#if>
        </#list>
    </#if>
</#list>
-->

/**
 *
 * 表备注：${introspectedTable.remarks!}<#--deal null-->
 * @description 针对表【${dataBase}.${introspectedTable.fullyQualifiedTable.introspectedTableName}】的ServiceAbstract
 */
public abstract class ${className}ServiceAbstract extends ServiceImpl<${className}Mapper, ${className}> implements BaseService<${className}>, ${className}Service, ${className}ServiceAbstractService {

    protected ${className}Mapper ${classNameTuoFeng}Mapper;

    @Autowired
    public void set${className}Mapper(${className}Mapper ${classNameTuoFeng}Mapper) {
        this.${classNameTuoFeng}Mapper = ${classNameTuoFeng}Mapper;
    }


    @Override
    public int countBaseByMap(Map<SFunction<${className}, ?>, Object> map) {
        if (map == null) {
            return 0;
        }
        return BaseService.mapToQueryWrapper(map).map(super::count).map(Long::intValue).orElse(0);
    }
    
    @Override
    public Page<${className}> getBaseByMapPage(Page<${className}> page, Map<SFunction<${className}, ?>, Object> map) {
        if (page == null) {
            return new Page<>();
        }
        return BaseService.mapToQueryWrapper(map).map(qw -> super.page(page,qw)).orElse(new Page<>());
    }
    
    @Override
    public List<${className}> getBaseByMap(Map<SFunction<${className}, ?>, Object> map) {
        return BaseService.mapToQueryWrapper(map).map(super::list).orElse(Collections.emptyList());
    }
    
    @Override
    public Optional<${className}> getBaseByMapOneOp(Map<SFunction<${className}, ?>, Object> map) {
        List<${className}> list = this.getBaseByMap(map);
        if (list.isEmpty()) {
            return Optional.empty();
        }
        if (list.size() > 1) {
            throw new TooManyResultsException("Expected one result (or null) to be returned by selectOne(), but found: " + list.size());
        } else {
            return Optional.of(list.get(0));
        }
    }


    @Override
    public int countBaseByMapIgnoreNull(Map<SFunction<${className}, ?>, Object> map) {
        if (map == null) {
            return 0;
        }
        return BaseService.mapToQueryWrapper(map,false).map(super::count).map(Long::intValue).orElse(0);
    }

    @Override
    public Page<${className}> getBaseByMapPageIgnoreNull(Page<${className}> page, Map<SFunction<${className}, ?>, Object> map) {
        if (page == null) {
            return new Page<>();
        }
        return BaseService.mapToQueryWrapper(map,false).map(qw -> super.page(page,qw)).orElse(new Page<>());
    }

    @Override
    public List<${className}> getBaseByMapIgnoreNull(Map<SFunction<${className}, ?>, Object> map) {
        return BaseService.mapToQueryWrapper(map,false).map(super::list).orElse(Collections.emptyList());
    }

    @Override
    public Optional<${className}> getBaseByMapOneOpIgnoreNull(Map<SFunction<${className}, ?>, Object> map) {
        List<${className}> list = this.getBaseByMapIgnoreNull(map);
        if (list.isEmpty()) {
            return Optional.empty();
        }
        if (list.size() > 1) {
            throw new TooManyResultsException("Expected one result (or null) to be returned by selectOne(), but found: " + list.size());
        } else {
            return Optional.of(list.get(0));
        }
    }



}
