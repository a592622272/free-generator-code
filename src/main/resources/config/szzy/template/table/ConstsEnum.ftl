<#assign isHaveEnum=false /><#--定义局部变量-->
<#list introspectedTable.allColumns as allColumns>
    <#if allColumns.remarks!?length gt 0 && allColumns.remarks?index_of("#")!=-1>
        <#assign isHaveEnum=true />
        <#break>
    </#if>
</#list>
<#if isHaveEnum >
package ${(introspectedTable.tableConfiguration.properties.fileDirectoryEnum)?substring(((introspectedTable.tableConfiguration.properties.fileDirectoryEnum)?index_of("/src/main/java/")+15))?replace("/",".")};

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

/***
 * ---------------------------
 * @Description:
 * 表备注：${introspectedTable.remarks!}<#--deal null-->
 * 针对表【${introspectedTable.tableConfiguration.schema!}.${introspectedTable.fullyQualifiedTable.introspectedTableName}】的常量信息
 *
 * ---------------------------
 * @author: ${introspectedTable.tableConfiguration.properties.auther!}
 * @date: ${introspectedTable.tableConfiguration.properties.createTime!}
 * @version: v1.0
 * ---------------------------
 */
public class ${className}Enums implements Serializable {

<#-- ----------  BEGIN 字段循环遍历  ---------->
    <#list introspectedTable.allColumns as allColumns>
        <#if allColumns.remarks!?length gt 0>
            <#if allColumns.remarks?index_of("#")!=-1 >

    /***
     * ---------------------------
     * @Description: ${allColumns.remarks!}<#--${allColumns.remarks?substring(0,allColumns.remarks?index_of("#"))?trim}-->
     * ---------------------------
     * @author: ${introspectedTable.tableConfiguration.properties.auther!}
     * @date: ${introspectedTable.tableConfiguration.properties.createTime!}
     * @version: v1.0
     * ---------------------------
     */
    @Getter
    @AllArgsConstructor
    public enum ${allColumns.javaProperty?cap_first}Enum {
                    <#assign _isNumber=(allColumns.jdbcTypeName=="INTEGER"||allColumns.jdbcTypeName=="INT"||allColumns.jdbcTypeName=="NUMBER"||allColumns.jdbcTypeName=="TINYINT")?string("1","0")/>
                    <#assign _consts=allColumns.remarks?substring(allColumns.remarks?index_of("#")+1)/>

                <#if allColumns.remarks?index_of("$") gt 0>
                    <#assign paramNameStrs=allColumns.remarks?substring(allColumns.remarks?index_of("$")+1)/>
                    <#assign paramNameStrShuZu = paramNameStrs?split("$") />
                    <#assign paramNameStr = paramNameStrShuZu[0]?trim />

                    <#list _consts?split("#") as item>
                        <#assign itemEachs = item?split(",") />
        /** ${itemEachs[2]?trim} **/
                        <#if _isNumber=="1">
       <#list item?split(",") as itemEach><#if itemEach_index == 0> <#elseif itemEach_index == 1>(<#elseif itemEach_index == 2>,"<#else >","</#if>${(itemEach?trim)?upper_case}</#list>"),<#--(byte)后端规范要求TINYINT使用java中的integer,所以不加byte-->
                        <#else>
                            <#list item?split(",") as itemEach><#if itemEach_index == 0> <#elseif itemEach_index == 1>("<#elseif itemEach_index == 2>","<#else >","</#if>${(itemEach?trim)?upper_case}</#list>"),<#--(byte)后端规范要求TINYINT使用java中的integer,所以不加byte-->
                        </#if>
                    </#list>

        ;

        <#list paramNameStr?split(",") as itemField>
            <#if itemField_index == 0>
        private final ${allColumns.fullyQualifiedJavaType.shortNameWithoutTypeArguments} <#else >
        private final String </#if>${itemField?trim};</#list>

        <#list paramNameStr?split(",") as itemField><#if itemField_index == 0>public static final Map<${allColumns.fullyQualifiedJavaType.shortNameWithoutTypeArguments}, ${allColumns.javaProperty?cap_first}Enum> <#else >
        public static final Map<String, ${allColumns.javaProperty?cap_first}Enum> </#if>${itemField?trim}EnumMap = new LinkedHashMap<>();</#list>

        static {
            ${allColumns.javaProperty?cap_first}Enum[] enums = ${allColumns.javaProperty?cap_first}Enum.values();

            for (${allColumns.javaProperty?cap_first}Enum en : enums) {
            <#list paramNameStr?split(",") as itemField>
                //字段不唯一则会被覆盖
                ${itemField?trim}EnumMap.put(en.${itemField?trim}, en);
            </#list>
            }
        }

        <#list paramNameStr?split(",") as itemField>
        public static Optional<${allColumns.javaProperty?cap_first}Enum> getEnumBy${(itemField?trim)?cap_first}(${allColumns.fullyQualifiedJavaType.shortNameWithoutTypeArguments} ${itemField?trim}) {
            return Optional.ofNullable(${itemField?trim}EnumMap.get(${itemField?trim}));
        }
        </#list>

    }
                <#else>
                    <#list _consts?split("#") as item>
                        <#assign _subItem = item?split(",") />
                        <#if _subItem?size gte 3>
                            <#if _isNumber=="1">
        /** ${_subItem[2]?trim} **/
        ${(_subItem[0]?trim)?upper_case}(<#if allColumns.jdbcTypeName=="TINYINT"><#--(byte)后端规范要求TINYINT使用java中的integer--></#if>${_subItem[1]?trim}, "${_subItem[2]?trim}"),

                            <#else>
        /** ${_subItem[2]?trim} **/
        ${(_subItem[0]?trim)?upper_case}("${_subItem[1]?trim}", "${_subItem[2]?trim}"),

                            </#if>
                        </#if>
                    </#list>

        ;

            private final ${allColumns.fullyQualifiedJavaType.shortNameWithoutTypeArguments} code;
            private final String name;

            public static final Map<${allColumns.fullyQualifiedJavaType.shortNameWithoutTypeArguments}, ${allColumns.javaProperty?cap_first}Enum> codeEnumMap = new LinkedHashMap<>();
            public static final Map<String ,${allColumns.javaProperty?cap_first}Enum> nameEnumMap = new LinkedHashMap<>();

            static {
                ${allColumns.javaProperty?cap_first}Enum[] enums = ${allColumns.javaProperty?cap_first}Enum.values();

                for (${allColumns.javaProperty?cap_first}Enum en : enums) {
                    //字段不唯一则会被覆盖
                    codeEnumMap.put(en.code, en);
                    //字段不唯一则会被覆盖
                    nameEnumMap.put(en.name,en);
                }
            }

            public static Optional<${allColumns.javaProperty?cap_first}Enum> getEnumByCode(${allColumns.fullyQualifiedJavaType.shortNameWithoutTypeArguments} code) {
                return Optional.ofNullable(codeEnumMap.get(code));
            }

            public static Optional<${allColumns.javaProperty?cap_first}Enum> getEnumByName(String name) {
                return Optional.ofNullable(nameEnumMap.get(name));
            }
    }

                </#if>
            </#if>
        </#if>
    </#list>


}
<#else ></#if>