package ${fileDirectoryControllerPackage};

import ${fileDirectoryServicePackage}.${className}Service;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("${classNameTuoFeng}")
@Api(tags = BaseController.YJDAXT + "${introspectedTable.remarks!}<#--deal null-->")
@Slf4j
public class ${className}Controller extends BaseController {

    private ${className}Service ${classNameTuoFeng}Service;

    @Autowired
    public void set${className}Service(${className}Service ${classNameTuoFeng}Service) {
        this.${classNameTuoFeng}Service = ${classNameTuoFeng}Service;
    }



}
