package ${fileDirectoryEntityPackage};

<#assign hasPrimaryKey=false/><#--表是否有主键-->
<#if introspectedTable.primaryKeyColumns ?size &gt;0 >
    <#assign hasPrimaryKey=true/>
</#if>
<#if hasPrimaryKey>import com.baomidou.mybatisplus.annotation.TableId;</#if><#--有主键才导入包-->
import com.baomidou.mybatisplus.annotation.TableName;
import com.cjbdi.common.base.BaseVo;
import ${fileDirectoryEntityExtPackage}.${className}Ext;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

<#-- 导入包，去除重复包 -->
<#list introspectedTable.allColumns as allColumns>
<#assign paramType=allColumns.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters /><#--定义局部变量-->
    <#if paramType ?index_of("java.lang")==-1 && paramType ?index_of("[]")==-1><#--java.lang的包不需要导入，数组也不要导入，比如byte[] -->
        <#list introspectedTable.allColumns as allColumns2>
            <#if allColumns2.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters ?index_of("java.lang")!=0>
                <#if allColumns.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters ==allColumns2.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters>
                    <#if allColumns_index==allColumns2_index>
<#-- 导入包名 -->
import ${allColumns.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters};
                    <#else>
                        <#break >
                    </#if>
                </#if>
            </#if>
        </#list>
    </#if>
</#list>


/**
 *
 * 表备注：${introspectedTable.remarks!}<#--deal null-->
 * 表名：${dataBase}.${introspectedTable.fullyQualifiedTable.introspectedTableName}
 *
 */
@TableName("${dataBase}.${introspectedTable.fullyQualifiedTable.introspectedTableName}")
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class ${className} extends BaseVo implements Serializable {

    private static final long serialVersionUID = 1L;

<#-- ----------  BEGIN 字段循环遍历  ---------->
<#-- 主键字段属性 -->
<#list introspectedTable.primaryKeyColumns as primaryKey>
    /**db字段: ${primaryKey.actualColumnName}
     * ${primaryKey.remarks!} */
    @ApiModelProperty(value = "${primaryKey.remarks!}")
    @TableId
    private ${primaryKey.fullyQualifiedJavaType.shortNameWithoutTypeArguments} ${primaryKey.javaProperty};

</#list>

<#-- 非主键字段属性 -->
<#list introspectedTable.nonPrimaryKeyColumns as baseColumns>
    /**db字段: ${baseColumns.actualColumnName}
     * ${baseColumns.remarks!} */
    @ApiModelProperty(value = "${baseColumns.remarks!}")
<#--这里仅暂时兼容不符合规范的字段，（以下划线结尾的） ............. start.-->
    <#if baseColumns.actualColumnName?length-1==baseColumns.actualColumnName?last_index_of("_")>
    <#--这里仅暂时兼容不符合规范的字段，（以下划线结尾的），处理：结尾加下划线-->
    private ${baseColumns.fullyQualifiedJavaType.shortNameWithoutTypeArguments} ${baseColumns.javaProperty}_;
    <#else>
    private ${baseColumns.fullyQualifiedJavaType.shortNameWithoutTypeArguments} ${baseColumns.javaProperty};
    </#if>
<#--这里仅暂时兼容不符合规范的字段，（以下划线结尾的） ............. end.-->

</#list>

<#-- get、set方法 -->
<#list introspectedTable.allColumns as allColumn>
    <#assign methedProperty="" /><#--定义局部变量-->
    <#if (allColumn.actualColumnName?length>2)&&(allColumn.actualColumnName?substring(1,2)=="_") >
        <#-- 兼容x_xx类字段，比如：N_FYDM 对应实体get方法为 getnFydm，为了和ide、spring的规范保持一致。-->
        <#assign methedProperty=allColumn.javaProperty />
    <#else>
        <#assign methedProperty=allColumn.javaProperty?cap_first />
    </#if>
<#--这里仅暂时兼容不符合规范的字段，（以下划线结尾的） ............. start.-->
    <#if allColumn.actualColumnName?length-1==allColumn.actualColumnName?last_index_of("_")>
    <#--这里仅暂时兼容不符合规范的字段，（以下划线结尾的），处理：结尾加下划线-->
        <#assign methedProperty=methedProperty+"_" />
    </#if>
<#--这里仅暂时兼容不符合规范的字段，（以下划线结尾的） ............. end.-->
    public ${allColumn.fullyQualifiedJavaType.shortNameWithoutTypeArguments} get${methedProperty}() {
        return ${allColumn.javaProperty}; //${allColumn.remarks!}
    }

    @SuppressWarnings("unchecked")
    public <E extends ${className}> E set${methedProperty}(${allColumn.fullyQualifiedJavaType.shortNameWithoutTypeArguments} ${allColumn.javaProperty}) {
        this.${allColumn.javaProperty} = ${allColumn.javaProperty}; //${allColumn.remarks!}
        return (E) this;
    }

</#list>

    /**转换到扩展类 */
    public ${className}Ext convertToExt() {
        ${className}Ext ext = new ${className}Ext();
    <#list introspectedTable.allColumns as allColumn>
        <#assign methedProperty="" /><#--定义局部变量-->
        <#if (allColumn.actualColumnName?length>2)&&(allColumn.actualColumnName?substring(1,2)=="_") >
            <#-- 兼容x_xx类字段，比如：N_FYDM 对应实体get方法为 getnFydm，为了和ide、spring的规范保持一致。-->
            <#assign methedProperty=allColumn.javaProperty />
        <#else>
            <#assign methedProperty=allColumn.javaProperty?cap_first />
        </#if>
        ext.set${methedProperty}(this.${allColumn.javaProperty});
    </#list>
        return ext;
    }


}
