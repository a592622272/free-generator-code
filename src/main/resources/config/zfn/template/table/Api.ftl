package ${(introspectedTable.tableConfiguration.properties.fileDirectoryApi)?substring(((introspectedTable.tableConfiguration.properties.fileDirectoryApi)?index_of("/src/main/java/")+15))?replace("/",".")};


import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;


/***
 * ---------------------------
 * @Description: 表备注：${introspectedTable.remarks!}<#--deal null-->
 * 针对表【${dataBase}.${introspectedTable.fullyQualifiedTable.introspectedTableName}】的Api
 * ---------------------------
 * @auther: ${introspectedTable.tableConfiguration.properties.auther!}
 * @date: ${introspectedTable.tableConfiguration.properties.createTime!}
 * @version: v1.0
 * ---------------------------
 */
@FeignClient(name = ApiConstants.NAME,contextId = "${className}Api")
@Tag(name = "RPC 服务 - ${introspectedTable.fullyQualifiedTable.introspectedTableName} - ${introspectedTable.remarks!}")
public interface ${className}Api {

    String PREFIX = ApiConstants.PREFIX + "/${introspectedTable.fullyQualifiedTable.introspectedTableName?replace("_","-")}";



}
