package com.broker.orch.businessbackend.controller.admin.agreement;

import ${(introspectedTable.tableConfiguration.properties.fileDirectoryOrchService)?substring(((introspectedTable.tableConfiguration.properties.fileDirectoryOrchService)?index_of("/src/main/java/")+15))?replace("/",".")}.${className}Service;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/***
 * ---------------------------
 * @Description: 表备注：${introspectedTable.remarks!}<#--deal null-->
 * 针对表【${dataBase}.${introspectedTable.fullyQualifiedTable.introspectedTableName}】的Controller
 * ---------------------------
 * @auther: ${introspectedTable.tableConfiguration.properties.auther!}
 * @date: ${introspectedTable.tableConfiguration.properties.createTime!}
 * @version: v1.0
 * ---------------------------
 */
@Tag(name = "${introspectedTable.remarks!}")
@RestController
@Validated
@RequestMapping("/${introspectedTable.fullyQualifiedTable.introspectedTableName?replace("_","-")}")
@Slf4j
public class ${className}Controller {

    @Resource
    private ${className}Service ${classNameTuoFeng}Service;

}
