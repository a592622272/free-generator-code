package ${(introspectedTable.tableConfiguration.properties.fileDirectoryApiImpl)?substring(((introspectedTable.tableConfiguration.properties.fileDirectoryApiImpl)?index_of("/src/main/java/")+15))?replace("/",".")};

import ${(introspectedTable.tableConfiguration.properties.fileDirectoryService)?substring(((introspectedTable.tableConfiguration.properties.fileDirectoryService)?index_of("/src/main/java/")+15))?replace("/",".")}.${className}Service;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/***
 * ---------------------------
 * @Description: 表备注：${introspectedTable.remarks!}<#--deal null-->
 * 针对表【${dataBase}.${introspectedTable.fullyQualifiedTable.introspectedTableName}】的Api实现类
 * ---------------------------
 * @auther: ${introspectedTable.tableConfiguration.properties.auther!}
 * @date: ${introspectedTable.tableConfiguration.properties.createTime!}
 * @version: v1.0
 * ---------------------------
 */
@RestController
public class ${className}ApiImpl implements ${className}Api {

    @Resource
    private ${className}Service ${classNameTuoFeng}Service;


}
