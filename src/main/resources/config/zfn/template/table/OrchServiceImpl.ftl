package ${(introspectedTable.tableConfiguration.properties.fileDirectoryOrchServiceImpl)?substring(((introspectedTable.tableConfiguration.properties.fileDirectoryOrchServiceImpl)?index_of("/src/main/java/")+15))?replace("/",".")};

import ${(introspectedTable.tableConfiguration.properties.fileDirectoryApi)?substring(((introspectedTable.tableConfiguration.properties.fileDirectoryApi)?index_of("/src/main/java/")+15))?replace("/",".")}.${className}Api;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/***
 * ---------------------------
 * @Description: 表备注：${introspectedTable.remarks!}<#--deal null-->
 * 针对表【${dataBase}.${introspectedTable.fullyQualifiedTable.introspectedTableName}】的orch ServiceImpl
 * ---------------------------
 * @auther: ${introspectedTable.tableConfiguration.properties.auther!}
 * @date: ${introspectedTable.tableConfiguration.properties.createTime!}
 * @version: v1.0
 * ---------------------------
 */
@Service
public class ${className}ServiceImpl implements ${className}Service {

    @Resource
    private ${className}Api ${classNameTuoFeng}Api;


}
