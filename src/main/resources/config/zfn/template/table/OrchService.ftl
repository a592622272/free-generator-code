package ${(introspectedTable.tableConfiguration.properties.fileDirectoryOrchService)?substring(((introspectedTable.tableConfiguration.properties.fileDirectoryOrchService)?index_of("/src/main/java/")+15))?replace("/",".")};


/***
 * ---------------------------
 * @Description: 表备注：${introspectedTable.remarks!}<#--deal null-->
 * 针对表【${dataBase}.${introspectedTable.fullyQualifiedTable.introspectedTableName}】的orch Service
 * ---------------------------
 * @auther: ${introspectedTable.tableConfiguration.properties.auther!}
 * @date: ${introspectedTable.tableConfiguration.properties.createTime!}
 * @version: v1.0
 * ---------------------------
 */
public interface ${className}Service {

}
