package ${(introspectedTable.tableConfiguration.properties.fileDirectoryServiceImpl)?substring(((introspectedTable.tableConfiguration.properties.fileDirectoryServiceImpl)?index_of("/src/main/java/")+15))?replace("/",".")};

import ${(introspectedTable.tableConfiguration.properties.fileDirectoryMapper)?substring(((introspectedTable.tableConfiguration.properties.fileDirectoryMapper)?index_of("/src/main/java/")+15))?replace("/",".")}.${className}Mapper;
import javax.annotation.Resource;

/***
 * ---------------------------
 * @Description: 表备注：${introspectedTable.remarks!}<#--deal null-->
 * 针对表【${dataBase}.${introspectedTable.fullyQualifiedTable.introspectedTableName}】的ServiceImpl
 * ---------------------------
* @auther: ${introspectedTable.tableConfiguration.properties.auther!}
* @date: ${introspectedTable.tableConfiguration.properties.createTime!}
 * @version: v1.0
 * ---------------------------
 */
public class ${className}ServiceImpl implements ${className}Service {

    @Resource
    private ${className}Mapper ${classNameTuoFeng}Mapper;



}




