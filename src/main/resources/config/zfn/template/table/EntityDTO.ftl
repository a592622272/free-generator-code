package ${(introspectedTable.tableConfiguration.properties.fileDirectoryDTO)?substring(((introspectedTable.tableConfiguration.properties.fileDirectoryDTO)?index_of("/src/main/java/")+15))?replace("/",".")};

<#assign hasPrimaryKey=false/><#--表是否有主键-->
<#if introspectedTable.primaryKeyColumns ?size &gt;0 >
    <#assign hasPrimaryKey=true/>
</#if>
import ${(introspectedTable.tableConfiguration.properties.fileDirectoryBaseDTO)?substring(((introspectedTable.tableConfiguration.properties.fileDirectoryBaseDTO)?index_of("/src/main/java/")+15))?replace("/",".")}.${className}BaseDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

<#-- 导入包，去除重复包 -->
<#list introspectedTable.allColumns as allColumns>
<#assign paramType=allColumns.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters /><#--定义局部变量-->
    <#if paramType ?index_of("java.lang")==-1 && paramType ?index_of("[]")==-1><#--java.lang的包不需要导入，数组也不要导入，比如byte[] -->
        <#list introspectedTable.allColumns as allColumns2>
            <#if allColumns2.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters ?index_of("java.lang")!=0>
                <#if allColumns.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters ==allColumns2.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters>
                    <#if allColumns_index==allColumns2_index>
<#-- 导入包名 -->
import ${allColumns.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters};
                    <#else>
                        <#break >
                    </#if>
                </#if>
            </#if>
        </#list>
    </#if>
</#list>

/***
 * ---------------------------
 * @Description:
 * 表备注：${introspectedTable.remarks!}<#--deal null-->
 * 数据库：${dataBase}
 * 表名：${introspectedTable.fullyQualifiedTable.introspectedTableName}
 * ---------------------------
 * @auther: ${introspectedTable.tableConfiguration.properties.auther!}
 * @date: ${introspectedTable.tableConfiguration.properties.createTime!}
 * @version: v1.0
 * ---------------------------
 */
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
@Schema(description = "${introspectedTable.remarks!}")
public class ${className}DTO extends ${className}BaseDTO {


}
