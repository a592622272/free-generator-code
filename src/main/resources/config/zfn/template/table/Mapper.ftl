package ${(introspectedTable.tableConfiguration.properties.fileDirectoryMapper)?substring(((introspectedTable.tableConfiguration.properties.fileDirectoryMapper)?index_of("/src/main/java/")+15))?replace("/",".")};

import com.broker.framework.mybatis.core.mapper.BaseMapperX;
import ${(introspectedTable.tableConfiguration.properties.fileDirectoryDO)?substring(((introspectedTable.tableConfiguration.properties.fileDirectoryDO)?index_of("/src/main/java/")+15))?replace("/",".")}.${className}DO;
import org.apache.ibatis.annotations.Mapper;

/***
 * ---------------------------
 * @Description: 表备注：${introspectedTable.remarks!}<#--deal null-->
 * 针对表【${dataBase}.${introspectedTable.fullyQualifiedTable.introspectedTableName}】的Mapper
 * ---------------------------
* @auther: ${introspectedTable.tableConfiguration.properties.auther!}
* @date: ${introspectedTable.tableConfiguration.properties.createTime!}
 * @version: v1.0
 * ---------------------------
 */
@Mapper
public interface ${className}Mapper extends BaseMapperX<${className}DO> {


}




