package ${(introspectedTable.tableConfiguration.properties.fileDirectoryBaseDTO)?substring(((introspectedTable.tableConfiguration.properties.fileDirectoryBaseDTO)?index_of("/src/main/java/")+15))?replace("/",".")};

<#assign hasPrimaryKey=false/><#--表是否有主键-->
<#if introspectedTable.primaryKeyColumns ?size &gt;0 >
    <#assign hasPrimaryKey=true/>
</#if>
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

<#-- 导入包，去除重复包 -->
<#list introspectedTable.allColumns as allColumns>
<#assign paramType=allColumns.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters /><#--定义局部变量-->
    <#if paramType ?index_of("java.lang")==-1 && paramType ?index_of("[]")==-1><#--java.lang的包不需要导入，数组也不要导入，比如byte[] -->
        <#list introspectedTable.allColumns as allColumns2>
            <#if allColumns2.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters ?index_of("java.lang")!=0>
                <#if allColumns.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters ==allColumns2.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters>
                    <#if allColumns_index==allColumns2_index>
<#-- 导入包名 -->
import ${allColumns.fullyQualifiedJavaType.fullyQualifiedNameWithoutTypeParameters};
                    <#else>
                        <#break >
                    </#if>
                </#if>
            </#if>
        </#list>
    </#if>
</#list>

/***
 * ---------------------------
 * @Description:
 * 表备注：${introspectedTable.remarks!}<#--deal null-->
 * 数据库：${dataBase}
 * 表名：${introspectedTable.fullyQualifiedTable.introspectedTableName}
 * 自动创建维护，勿手动修改
 * ---------------------------
 * @auther: ${introspectedTable.tableConfiguration.properties.auther!}
 * @date: ${introspectedTable.tableConfiguration.properties.createTime!}
 * @version: v1.0
 * ---------------------------
 */
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
@Schema(description = "${introspectedTable.remarks!}")
public class ${className}BaseDTO {

<#-- ----------  BEGIN 字段循环遍历  ---------->
<#-- 主键字段属性 -->
<#list introspectedTable.primaryKeyColumns as primaryKey>
    /**db字段: ${primaryKey.actualColumnName}
     * ${primaryKey.remarks!} */
    @Schema(description = "${primaryKey.remarks!}")
    private ${primaryKey.fullyQualifiedJavaType.shortNameWithoutTypeArguments} ${primaryKey.javaProperty};

</#list>

<#-- 非主键字段属性 -->
<#assign ignoreFields=",createTime,updateTime,creator,updater,deleted,"/><#--表是否有主键-->
<#list introspectedTable.nonPrimaryKeyColumns as baseColumns>
    <#if ignoreFields?index_of(baseColumns.javaProperty)==-1>
    /**db字段: ${baseColumns.actualColumnName}
     * ${baseColumns.remarks!} */
    @Schema(description = "${baseColumns.remarks!}")
<#--这里仅暂时兼容不符合规范的字段，（以下划线结尾的） ............. start.-->
    <#if baseColumns.actualColumnName?length-1==baseColumns.actualColumnName?last_index_of("_")>
    <#--这里仅暂时兼容不符合规范的字段，（以下划线结尾的），处理：结尾加下划线-->
    private ${baseColumns.fullyQualifiedJavaType.shortNameWithoutTypeArguments} ${baseColumns.javaProperty}_;
    <#else>
    private ${baseColumns.fullyQualifiedJavaType.shortNameWithoutTypeArguments} ${baseColumns.javaProperty};
    </#if>
<#--这里仅暂时兼容不符合规范的字段，（以下划线结尾的） ............. end.-->

    </#if>
</#list>

<#-- get、set方法 -->
<#list introspectedTable.allColumns as allColumn>

<#if ignoreFields?index_of(allColumn.javaProperty)==-1>

    <#assign methedProperty="" /><#--定义局部变量-->
    <#if (allColumn.actualColumnName?length>2)&&(allColumn.actualColumnName?substring(1,2)=="_") >
        <#-- 兼容x_xx类字段，比如：N_FYDM 对应实体get方法为 getnFydm，为了和ide、spring的规范保持一致。-->
        <#assign methedProperty=allColumn.javaProperty />
    <#else>
        <#assign methedProperty=allColumn.javaProperty?cap_first />
    </#if>
<#--这里仅暂时兼容不符合规范的字段，（以下划线结尾的） ............. start.-->
    <#if allColumn.actualColumnName?length-1==allColumn.actualColumnName?last_index_of("_")>
    <#--这里仅暂时兼容不符合规范的字段，（以下划线结尾的），处理：结尾加下划线-->
        <#assign methedProperty=methedProperty+"_" />
    </#if>
<#--这里仅暂时兼容不符合规范的字段，（以下划线结尾的） ............. end.-->
    public ${allColumn.fullyQualifiedJavaType.shortNameWithoutTypeArguments} get${methedProperty}() {
        return ${allColumn.javaProperty}; //${allColumn.remarks!}
    }

    @SuppressWarnings("unchecked")
    public <E extends ${className}BaseDTO> E set${methedProperty}(${allColumn.fullyQualifiedJavaType.shortNameWithoutTypeArguments} ${allColumn.javaProperty}) {
        this.${allColumn.javaProperty} = ${allColumn.javaProperty}; //${allColumn.remarks!}
        return (E) this;
    }
    </#if>
</#list>


}
