package ${(introspectedTable.tableConfiguration.properties.fileDirectoryService)?substring(((introspectedTable.tableConfiguration.properties.fileDirectoryService)?index_of("/src/main/java/")+15))?replace("/",".")};

/***
 * ---------------------------
 * @Description: 表备注：${introspectedTable.remarks!}<#--deal null-->
 * 针对表【${dataBase}.${introspectedTable.fullyQualifiedTable.introspectedTableName}】的Service
 * ---------------------------
* @auther: ${introspectedTable.tableConfiguration.properties.auther!}
* @date: ${introspectedTable.tableConfiguration.properties.createTime!}
 * @version: v1.0
 * ---------------------------
 */
public interface ${className}Service {


}




