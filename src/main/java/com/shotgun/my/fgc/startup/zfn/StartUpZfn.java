package com.shotgun.my.fgc.startup.zfn;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;
import org.springframework.core.io.ClassPathResource;

import java.util.ArrayList;
import java.util.List;

public class StartUpZfn {
    public static void main(String[] args) throws Exception {
        List<String> warnings = new ArrayList<>();

        ClassPathResource generatorConfigResource = new ClassPathResource("config/zfn/generatorConfig.xml");
//		File configFile = new File("generatorConfig.xml");
        ConfigurationParser cp = new ConfigurationParser(warnings);
        Configuration config = cp.parseConfiguration(generatorConfigResource.getInputStream());
        DefaultShellCallback callback = new DefaultShellCallback(true);
        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
        myBatisGenerator.generate(null);

        System.out.println(String.join("\n", warnings));
        System.out.println(StartUpZfn.class.getSimpleName()+"........................end.");

    }
}
