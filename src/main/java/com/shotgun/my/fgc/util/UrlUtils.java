package com.shotgun.my.fgc.util;

public class UrlUtils {

    public static String getDataBaseFromConnectionURL(String connectionURL) {
        String dataBaseName;

        String lastIndexStr = "/";
        int lastIndexStrIndex = connectionURL.lastIndexOf(lastIndexStr);
        if (lastIndexStrIndex >= 0) {
            dataBaseName = connectionURL.substring(lastIndexStrIndex + lastIndexStr.length());
        } else {
            dataBaseName = connectionURL;
        }

        String indexOftr = "?";
        int indexOftrIndex = dataBaseName.indexOf(indexOftr);

        if (indexOftrIndex >= 0) {
            dataBaseName = dataBaseName.substring(0, indexOftrIndex);
        }

        return dataBaseName.trim();
    }

}
