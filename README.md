# free-generator-code

基于FreeMarker、mybatis-generator的通用代码生成器，生成不仅限于java代码，还可生成任意你想要的语言代码。使用非常简单，你几乎不需要编写java代码，内部包含许多ftl案例，你可以参考ftl案例来扩展自定义ftl模板。
<hr>

# 框架原理:
    1. 将mybatis-generator中的运行时变量（表结构等对象）传递给FreeMarker
    2. 解析并执行FreeMarker模板生成代码。
    2.1. 解析时自动扫描“表”模板(一个表执行一次模板文件)和“属性”模板(表的每个字段执行一次模板文件)
    2.2. 根据generatorConfig.xml中配置要生成的表进行处理
<hr>

# 结构介绍：
+ **文件类型分类：**
  + StartUp*.java 代码生成启动类
  + FgcPlugin.java 生成器核心插件，由mybatis-generator加载
  + generatorConfig.xml 配置文件
    + 设置要生成的表、设置配置和自定义属性
    + 由“tableTemplatePackage”定义表模板文件
    + 由“fieldTemplatePackage”定义属性模板文件
  + generatorConfig.properties 属性配置文件
    + 会导入到generatorConfig.xml中
  + *.ftl 模板文件
  + *.ftl.config.properties 模板配置文件
    
<hr>

# 入门：
  ## 快速起步
    1. 更改样例“generatorConfig.properties”中的数据库配置
    2. 修改样例“generatorConfig.xml”中table标签以添加自己想要生成的数据库表
    3. 运行样例“StartUp*.java”文件以生成结果。
    4. 查看运行结果，文件目录存储位置在“*.ftl.config.properties”文件中filePath属性配置，通常变量的值存放在“generatorConfig.xml”文件下.

# 进阶：
## 模板默认参数说明
    1.xxx待完善

## 模板修改
    1. 参考现有的模板进行修改。

# 版本更新内容：
+ 4.0.0
  + 简化配置，新的开始
  

<hr>
  

